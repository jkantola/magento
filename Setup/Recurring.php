<?php

namespace Survey\SurveyPage\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class Recurring implements InstallSchemaInterface
{
    const ANSWER_TABLE = 'survey_answer';
    const ANSWER_ID = 'answer_id';
    const ANSWER_Q1 = 'question_1';
    const ANSWER_Q2 = 'question_2';
    const ANSWER_Q3 = 'question_3';
    const ANSWER_Q4 = 'question_4';

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $coreResource;

    /**
     * @param \Magento\Framework\App\ResourceConnection $coreResource
     */
    public function __construct(
        \Magento\Framework\App\ResourceConnection $coreResource
    ) {
        $this->coreResource = $coreResource;
    }

    /**
     * Stub for recurring setup script.
     *
     * For quick development and prototyping purposes we re-create our tables during every call to setup:upgrade,
     * regardless of version upgrades, which you normally would not do in production.
     *
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $setup
     * @throws
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $connection = $this->coreResource->getConnection();

        // Drop existing tables for quick prototyping. Comment out if you don't want this!
        if ($connection->isTableExists(self::ANSWER_TABLE)) {
            $connection->dropTable(self::ANSWER_TABLE);
        }

        $surveyTable = $connection->newTable(
            self::ANSWER_TABLE
        )->addColumn(
            self::ANSWER_ID,
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Answer ID'
                
        

            // build up your DB schema here
        )->addColumn(
                self::ANSWER_Q1,
                Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Sex' 
        )->addColumn(
                self::ANSWER_Q2,
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false, 'unsigned' => true],
                'Age'
        )->addColumn(
                self::ANSWER_Q3,
                Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Drink'
        )->addColumn(
                self::ANSWER_Q4,
                Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Habits'
                );
        

        $connection->createTable($surveyTable);

        $setup->endSetup();
    }
}