<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


namespace Survey\SurveyPage\Block;

use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \Magento\Catalog\Model\ProductRepository;
//use \Toptal\Blog\Model\ResourceModel\Post\Collection as PostCollection;
//use \Toptal\Blog\Model\ResourceModel\Post\CollectionFactory as PostCollectionFactory;
//use \Toptal\Blog\Model\Post;

class FormTemplateBlock extends Template
{
    /**
     * CollectionFactory
     * @var null|CollectionFactory
     */
    protected $_repository = null;
 
    /**
     * Constructor
     *
     * @param Context $context
     * @param PostCollectionFactory $postCollectionFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        ProductRepository $repository,
        array $data = []
    ) {
        $this->_repository = $repository;
        parent::__construct($context, $data);
    }

    /**
     * @return Post[]
     */
    public function getProduct($id)
    {
       return $this->_repository->getById($id);
    }


}