<?php
namespace Survey\SurveyPage\Model;

use \Magento\Framework\Model\AbstractModel;
use \Magento\Framework\DataObject\IdentityInterface;
use \Survey\SurveyPage\Api\Data\AnswerInterface;

class AnswerModel extends AbstractModel implements AnswerInterface, IdentityInterface{
    
    const CACHE_TAG = 'survey-questions';
    
    protected function _construct()
    {
        $this->_init('Survey\SurveyPage\Models\ResourceModel\AnswerModel');
    }
    
    public function getId(){
        return $this->getData(self::ANSWER_ID);
    }
    
    public function getQuestion1(){
        return $this->getData(self::QUESTION_1);
    }
    
    public function getQuestion2(){
        return $this->getData(self::QUESTION_2);
    }
    
    public function getQuestion3(){
        return $this->getData(self::QUESTION_3);
    }
    
    public function getQuestion4(){
        return $this->getData(self::QUESTION_4);
    }
    
    public function setId($id){
        return $this->setData(self::ANSWER_ID, $id);
    }
    
    public function setQuestion1($q1){
        return $this->setData(self::QUESTION_1, $q1);
    }
    
    public function setQuestion2($q2){
        return $this->setData(self::QUESTION_2, $q2);
    }
    
    public function setQuestion3($q3){
        return $this->setData(self::QUESTION_3, $q3);
    }
    
    public function setQuestion4($q4){
        return $this->setData(self::QUESTION_4, $q4);
    }
     public function getIdentities() {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
   
}

