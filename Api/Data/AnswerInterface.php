<?php
/*
 * AnswerInterface 
 */
namespace Survey\SurveyPage\Api\Data;

interface AnswerInterface{
    const ANSWER_ID = 'answer_id';
    const ANSWER_Q1 = 'question_1';
    const ANSWER_Q2 = 'question_2';
    const ANSWER_Q3 = 'question_3';
    const ANSWER_Q4 = 'question_4';
    
    public function getId();
    public function getQuestion1();
    public function getQuestion2();
    public function getQuestion3();
    public function getQuestion4();
    
    public function setId($id);
    public function setQuestion1($question1);
    public function setQuestion2($question2);
    public function setQuestion3($question3);
    public function setQuestion4($question4);
}
